/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotorEncoder.hpp
 * Author: user
 *
 * Created on March 22, 2017, 7:38 PM
 */

#ifndef MOTOR_ENCODER_HPP
#define MOTOR_ENCODER_HPP
#include "MotorDriver.hpp"

class MotorEncoder {
public:
    MotorEncoder();
    MotorEncoder(const MotorEncoder& orig);
    MotorEncoder(int pwmGpio1,
            int a1_gpio, int a2_gpio,
            int pwmGpio2,
            int b1_gpio, int b2_gpio,
            int outputA1_gpio, int outputB1_gpio,
            int outputA2_gpio, int outputB2_gpio);
    long readCounter1();
    long readCounter2();
    virtual ~MotorEncoder();

    void forward() {
        d1.forward();
        d2.forward();
    }

    void backward() {
        d1.backward();
        d2.backward();
    }

    void stop() {
        d1.stop();
        d2.stop();
    }
private:
    MotorDriver d1;
    MotorDriver d2;
    GPIOClass outputA1;
    GPIOClass outputB1;
    GPIOClass outputA2;
    GPIOClass outputB2;
    volatile long counter[2];
    int state[2];
    int lastState[2];

};

#endif /* MOTOR_ENCODER_HPP */

