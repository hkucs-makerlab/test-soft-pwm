/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotorDriver.cpp
 * Author: user
 * 
 * Created on March 22, 2017, 2:27 PM
 */

#include "MotorDriver.hpp"

MotorDriver::MotorDriver() {

}

MotorDriver::MotorDriver(const MotorDriver& orig) {
}

MotorDriver::MotorDriver(int pwmGpio, int a1_gpio, int a2_gpio) : SoftPWM(pwmGpio), a1_in(a1_gpio), a2_in(a2_gpio) {
    setFrequency(1000);
    setDutyCyclePercentage(100);
    a1_in.outputMode();
    a2_in.outputMode();

    stop();
}

void MotorDriver::forward() {
    a1_in.setHigh();
    a2_in.setLow();

}

void MotorDriver::backward() {
    a1_in.setLow();
    a2_in.setHigh();

}

void MotorDriver::stop() {
    a1_in.setLow();
    a2_in.setLow();
}

MotorDriver::~MotorDriver() {
    stop();
}

