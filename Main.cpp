#include <iostream>
#ifdef __cplusplus
extern "C" {
#endif
#include <unistd.h>
#include <sys/time.h>
#ifdef __cplusplus
}
#endif

#include "ServoClass.hpp"
#include "LEDClass.hpp"
#include "MotorDriver.hpp"
#include "MotorEncoder.hpp"
#include "RotaryEncoder.hpp"
using namespace std;

/*
 * 
 */
#define TIME_INTERVAL 2000

inline long millis() {
    struct timeval millis;
    gettimeofday(&millis, NULL);
    return (millis.tv_usec + millis.tv_sec * 1000000) / 1000;
}

int servo(int argc, char** argv) {
    ServoClass servo(21); // p40

    //servo.setAngle(0);
    servo.setDutyCyclePercentage(3.5);
    servo.delay(500);
    servo.setAngle(90);
    servo.delay(500);
    //servo.setAngle(180);
    servo.setDutyCyclePercentage(15.5);
    servo.delay(500);

    for (int i = 0; i < 180; i++) {
        servo.setAngle(i);
        servo.delay(50);
    }
    return 0;
}

int led(int argc, char** argv) {
    LEDClass led(21); //p40

    int i;
    for (i = 0; i <= 60; i++) {
        led.setDutyCyclePercentage(i);
        led.delay(50);
    }
    for (; i >= 0; i--) {
        led.setDutyCyclePercentage(i);
        led.delay(50);
    }
    sleep(2);

}

int motorDriver(int argc, char** argv) {
    GPIOClass motorDrvStdy(25); //p22
    motorDrvStdy.outputMode();
    motorDrvStdy.setHigh();

    MotorDriver motor(
            /*
                    18, //p12, a motor pwm gpio          
                    17, //p11, motor driver a1 gpio
                    27 //p13, motor driver a2 gpio
             */
            24, //p18, b motor pwm gpio
            22, //p15, motor driver b1 gpio
            23 //p16, motor driver b2 gpio
            );
    motor.forward();
    sleep(3);
    motor.setDutyCyclePercentage(50);
    motor.backward();
    sleep(3);
    motor.stop();
}

int twoMotorDrivers(int argc, char** argv) {
    GPIOClass motorDrvStdy(25); //p22
    motorDrvStdy.outputMode();
    motorDrvStdy.setHigh();

    MotorDriver motor1(
            18, //p12, a motor pwm gpio          
            17, //p11, motor driver a1 gpio
            27 //p13, motor driver a2 gpio
            );
    MotorDriver motor2(
            24, //p18, b motor pwm gpio
            22, //p15, motor driver b1 gpio
            23 //p16, motor driver b2 gpio
            );


    motor1.forward();
    motor2.forward();
    sleep(3);
    motor1.setDutyCyclePercentage(50);
    motor2.setDutyCyclePercentage(50);
    motor1.backward();
    motor2.backward();
    sleep(3);

    motor1.stop();
    motor2.stop();
}

void motorEncoder(int argc, char** argv) {
    unsigned long cur_time, prev_time;
    long prev_counter1, counter1;
    long prev_counter2, counter2;


    GPIOClass motorDrvStdy(25); // p22
    motorDrvStdy.outputMode();
    motorDrvStdy.setHigh();

    MotorEncoder motor(
            18, //p12, a motor pwm gpio          
            17, //p11, motor driver a1 gpio
            27, //p13, motor driver a2 gpio
            24, //p18, b motor pwm gpio
            22, //p15, motor driver b1 gpio
            23, //p16, motor driver b2 gpio
            5, //p29, motor encoder a1 gpio
            6, //p31, motor encoder a2 gpio
            13, //p33, motor encoder b1 gpio
            19 //p35, motor encoder b2 gpio
            );

    motor.forward();

    prev_time = millis();
    prev_counter1 = -99;
    do {
        counter1 = motor.readCounter1();
        counter2 = motor.readCounter2();
        if (counter1 != prev_counter1) std::cout << "1 -> " << counter1 << "\r\n";
        if (counter2 != prev_counter2) std::cout << "2 -> " << counter2 << "\r\n";
        prev_counter1 = counter1;
        prev_counter2 = counter2;
    } while (0 || (cur_time = millis()) < prev_time + TIME_INTERVAL);

    motor.stop();
}

void rotaryEncoder(int argc, char** argv) {
    unsigned long cur_time, prev_time;
    long prev_counter1 = -99, counter1;
    long prev_counter2 = -99, counter2;


    GPIOClass motorDrvStdy(25); //p22
    motorDrvStdy.outputMode();
    motorDrvStdy.setHigh();

    MotorDriver motor1(
            18, //p12, a motor pwm gpio          
            17, //p11, motor driver a1 gpio
            27 //p13, motor driver a2 gpio
            );
    MotorDriver motor2(
            24, //p18, b motor pwm gpio
            22, //p15, motor driver b1 gpio
            23 //p16, motor driver b2 gpio
            );
    RotaryEncoder encoder1(
            5, //p29, motor encoder a1 gpio
            6 //p31, motor encoder a2 gpio
            );
    RotaryEncoder encoder2(
            13, //p33, motor encoder b1 gpio
            19 //p35, motor encoder b2 gpio
            );

    motor1.forward();
    motor2.forward();
    prev_time = millis();
    do {
        counter1 = encoder1.readCounter();
        counter2 = encoder2.readCounter();
        if (counter1 != prev_counter1) std::cout << "1 -> " << counter1 << "\r\n";
        if (counter2 != prev_counter2) std::cout << "2 -> " << counter2 << "\r\n";
        prev_counter1 = counter1;
        prev_counter2 = counter2;
    } while (0 || ((cur_time = millis()) < prev_time + TIME_INTERVAL));
    motor1.stop();
    motor2.stop();
}

int main(int argc, char** argv) {
    //motorDriver(argc, argv);
    twoMotorDrivers(argc, argv);
    //rotaryEncoder(argc, argv);
    //motorEncoder(argc, argv);
    //led(argc,argv);
    //servo(argc,argv);
}