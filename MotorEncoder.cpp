/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotorEncoder.cpp
 * Author: user
 * 
 * Created on March 22, 2017, 7:38 PM
 */

#include "MotorEncoder.hpp"

MotorEncoder::MotorEncoder() {
}

MotorEncoder::MotorEncoder(const MotorEncoder& orig) {
}

MotorEncoder::MotorEncoder(int pwmGpio1,
        int a1_gpio, int a2_gpio,
        int pwmGpio2,
        int b1_gpio, int b2_gpio,
        int outputA1_gpio, int outputB1_gpio, int outputA2_gpio, int outputB2_gpio) :
d1(pwmGpio1, a1_gpio, a2_gpio),
d2(pwmGpio2, b1_gpio, b2_gpio),
outputA1(outputA1_gpio), outputB1(outputB1_gpio),
outputA2(outputA2_gpio), outputB2(outputB2_gpio),
counter{0, 0}
{
    outputA1.inputMode();
    outputB1.inputMode();
    outputA2.inputMode();
    outputB2.inputMode();
    lastState[0] = outputA1.getState();
    lastState[1] = outputA2.getState();
}

long MotorEncoder::readCounter1() {
    state[0] = outputA1.getState();
    if (state[0] != lastState[0]) {
        if (outputB1.getState() != state[0]) {
            counter[0]++;
        } else {
            counter[0]--;
        }
    }
    lastState[0] = state[0];
    return counter[0];
}

long MotorEncoder::readCounter2() {
    state[1] = outputA2.getState();
    if (state[1] != lastState[1]) {
        if (outputB2.getState() != state[1]) {
            counter[1]++;
        } else {
            counter[1]--;
        }
    }
    lastState[1] = state[1];
    return counter[1];
}

MotorEncoder::~MotorEncoder() {
}

